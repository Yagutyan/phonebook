ტელეფონების წიგნაკი
რეგისტრაცია / ავტორიზაცია / პაროლის აღდგენა
ტელეფონების წიგნაკი
დამატება / რედაქტირება / წაშლა
ჩანაწერი შეიცავს (სახელი, გვარი, ტელ.ნომერი)
ძებნა უნდა შევძლო (სახელი, გვარი, ტელეფონი) თ
UI/UX შენი გემოვნებით.

ტექნოლოგიები რომლებიც უნდა გამოიყენო
NodeJS (Express) ან Java (Spring Boot)
არარელაციური (noSQL) ან  რელაციური SQL მონაცემთა ბაზა
Angular 1 ან Angular 2/4/5


Generate Certificates

Generate server key and self signed server certificate
keytool -genkey -alias serverkey -keyalg RSA -storetype PKCS12 -keystore serverkeystore.p12 -ext SAN=dns:abc.com,dns:localhost,ip:127.0.0.1

Generate client key and self signed client certificate
keytool -genkey -alias clientkey -keyalg RSA -storetype PKCS12 -keystore clientkeystore.p12 -ext SAN=dns:def.com,dns:localhost,ip:127.0.0.1

Export the server certificate
keytool -export -alias serverkey -file servercert.cer -keystore serverkeystore.p12

Export the client certificate
keytool -export -alias clientkey -file clientcert.cer -keystore clientkeystore.p12

Import cert to $JAVA_HOME/jre/lib/security
sudo keytool -import -trustcacerts -alias localhost -file localhost.crt -keystore $JAVA_HOME/jre/lib/security/cacerts

test 123131